# swclock-offset
Some devices have a working but non-writable real-time clock (RTC).
This repository contains two shell scripts: One writes the offset between
'hwclock' and 'swclock' to a file at shutdown, another one reads the
offset from the file at boot and sets the 'swclock'. This way the system
time in userspace is kept in present time.

## Build
The repository contains a simple Makefile. By `make install` the two
shell scripts get installed to `/usr/bin`. Variable `DESTDIR` allows to change
the install path to something like `DESTDIR=$pkgdir`.

Both the OpenRC and systemd service files are installed by `make install`.
Downstream packaging can choose to distribute one set, both, or none.

## Managing OpenRC services
After installing the files, OpenRC services need to be set accordingly.

### Replace service hwclock by osclock
The service "osclock" is a dummy service simply providing "clock".
This avoids other services that need "clock" to call the service
"hwclock".
```
rc-update -q del hwclock boot
rc-update -q add osclock boot
```

### Assign swclock-offset services to runlevels "boot" and "shutdown"
The service "swclock-offset-boot" needs to run after the sysfs has
been mounted. As the sysfs is mounted in runlevel sysinit, assigning
the service to runlevel boot is enough to keep the order.
```
rc-update -q add swclock-offset-boot boot
rc-update -q add swclock-offset-shutdown shutdown
```

### Update dependency tree cache
Before installation of the package "swclock-offset", the system time
might jump back and forth. Because of this, OpenRC can get confused
whether the cached dependency tree is old or new. To avoid this
uncertainty, trigger an update of the dependency tree cache.
```
rc-update -q --update
```

### On package removal
Restore the original state of the services.
```
rc-update -q del swclock-offset-boot boot
rc-update -q del swclock-offset-shutdown shutdown

rc-update -q del osclock boot
rc-update -q add hwclock boot
```

## systemd service files
After installing the systemd service files they need to be set up accordingly.

### Reloading the daemon
Reload all systemd unit files to recreate the dependency tree.
```
systemctl daemon-reload
```

### Enabling the service
The systemd services needs to be enabled to allow them to run during boot and shutdown.
```
systemctl enable swclock-offset-boot
systemctl enable swclock-offset-shutdown
```

systemd only appears to synchronize the 'swclock' during initialization. As the `swclock-offset-boot` starts after systemd initialization there's no need to configure any other services.

### On package removal
Disable the systemd services from starting during boot and shutdown.
```
systemctl disable --now swclock-offset-boot
systemctl disable --now swclock-offset-shutdown
```

## Delete cache file and folder on package removal
Independent of the init system, on package removal the cache file and
its folder should be removed as a clean-up action. This can be done by
e.g.:

```
#!/bin/sh

offset_file="/var/cache/swclock-offset/offset-storage"
offset_directory="/var/cache/swclock-offset"

if [ -f $offset_file ]; then
	rm $offset_file
fi

if [ -d $offset_directory ]; then
	rmdir $offset_directory
fi
```
